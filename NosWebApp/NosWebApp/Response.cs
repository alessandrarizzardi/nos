﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Json;

namespace NosWebApp
{
   [DataContract]
   public class Response
   {
        
       [DataMember(Name = "_id")]
       public String ID{get; set;}

       [DataMember(Name = "sourceid")]
       public String sourceid { get; set; }

       [DataMember(Name = "sourcetype")]
       public String sourcetype { get; set; }
        
       [DataMember(Name = "timestamp")]
       public String TimeStamp { get; set;}
           
       [DataMember(Name = "temperature")]
       public String Temperature { get; set; }

       [DataMember(Name = "humidity")]
       public String Humidity { get; set; }

       [DataMember(Name = "wind")]
       public String Wind { get; set; }

       [DataMember(Name = "power")]
       public String Power { get; set; }

    }
}
