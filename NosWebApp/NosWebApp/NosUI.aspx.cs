﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Runtime.Serialization.Json;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Globalization;


namespace NosWebApp
{
    public partial class WebForm1 : System.Web.UI.Page
    {
        String confidentiality_string = "";
        String integrity_string = "";
        String privacy_string = "";
        String authenticity_string = "";
        String completeness_string = "";
        String freshness_string = "";
        String accuracy_string = "";
        //String ipNosPort = "192.168.1.144:3000";
        String ipNosPort = "localhost:3000";
        public String ScriptGrafico { get; set; }

        protected void Page_Load(object sender, EventArgs e)
        {
            ReadWebConfigXml();
           // this.ScriptGrafico = "<script>alert('page_load')</script>";
            this.CreateScript(new List<Response>());
            this.CreateScript2(new List<Response>());
            uiUpdate_lblConteggi();
           
        }

        protected void uiUpdate_lblConteggi() {
            ResponseCountService countData = (ResponseCountService)MakeRequest(CreateRequest_CountData(), typeof(ResponseCountService));
            ResponseCountService countRawData = (ResponseCountService)MakeRequest(CreateRequest_CountRawData(), typeof(ResponseCountService));
            lblCountData.Text = countData.Count;
            lblCountRawData.Text = countRawData.Count;
        }

        protected void cmdSubmit_Click(object sender, EventArgs e)
        {
             confidentiality_string = Confidentiality.Text;
             integrity_string = Integrity.Text;
             privacy_string = Privacy.Text;
             authenticity_string = Authenticity.Text;
             completeness_string = Completeness.Text;
             freshness_string = Freshness.Text;
             accuracy_string = Accuracy.Text;

             Response[] r = (Response[])MakeRequest(CreateRequest(Convert.ToInt32(confidentiality_string),Convert.ToInt32(integrity_string),
                                                                  Convert.ToInt32(privacy_string),Convert.ToInt32(authenticity_string),
                                                                  Convert.ToInt32(completeness_string), Convert.ToInt32(freshness_string),
                                                                  Convert.ToInt32(accuracy_string)), typeof(Response[]));
            
             List<Response> lResponse = new List<Response>();
             lResponse.AddRange(r);
             this.ScriptGrafico = "";
             if (CreateScript(lResponse) && CreateScript2(lResponse))
             {
                 pnlErrore.Visible = false;
                 System.Web.UI.ScriptManager.RegisterStartupScript(Page, this.GetType(), "pp", this.ScriptGrafico, true);
             }
             else {
                 pnlErrore.Visible = true;
             }
            // System.Web.UI.ScriptManager.RegisterStartupScript(Page, this.GetType(), "pp", "drawChart();", true);
        }

        public void ReadWebConfigXml() {
            System.Configuration.Configuration rootWebConfig1 = System.Web.Configuration.WebConfigurationManager.OpenWebConfiguration(null);
            if (rootWebConfig1.AppSettings.Settings.Count > 0)
            {
                System.Configuration.KeyValueConfigurationElement customSetting = rootWebConfig1.AppSettings.Settings["ipPortNos"];
                if (customSetting != null)
                    ipNosPort = customSetting.Value;
                else
                    ipNosPort = "NOSET";
            }
        }

        public String CreateRequest(int confidentiality, int integrity, int privacy,int authentication, int completeness, int freshness, int accuracy)
        {
            
           String url_string = "http://localhost:3000/get?confidentiality=" + confidentiality + "&integrity=" + integrity + "&privacy=" + privacy + 
                                "&authentication=" + authentication + "&completeness=" + completeness + "&freshness=" + freshness + "&accuracy=" + accuracy; //used for testing
           /*String url_string = "http://"+ipNosPort+"/get?confidentiality=" + confidentiality + "&integrity=" + integrity + "&privacy=" + privacy + 
                                 "&authentication=" + authentication + "&completeness=" + completeness + "&freshness=" + freshness + "&accuracy=" + accuracy;*/
            
           return url_string;
        }

        public String CreateRequest_CountData()
        {
           
            String url_string = "http://" + ipNosPort + "/getDataCount";
            return url_string;
        }

        public String CreateRequest_CountRawData()
        {
            
            String url_string = "http://" + ipNosPort + "/getDataCount";
            return url_string;
        }

        public object MakeRequest(string requestUrl,Type responseType)
        {
            try
            {
                HttpWebRequest request = WebRequest.Create(requestUrl) as HttpWebRequest;
                using (HttpWebResponse response = request.GetResponse() as HttpWebResponse)
                {
                    if (response.StatusCode != HttpStatusCode.OK)
                        return null;
                    DataContractJsonSerializer jsonSerializer = new DataContractJsonSerializer(responseType);
                    object objResponse = jsonSerializer.ReadObject(response.GetResponseStream());
                    return objResponse;
                }
            }
            catch (Exception e) {
                Console.WriteLine(e.Message);
                return null;
            }
        }

        public bool CreateScript(List<Response> list) {
            if(list ==null || list.Count==0){
                return false;
            }
            list.Sort((x, y) => DateTime.Compare(DateTime.ParseExact(x.TimeStamp, "dd/MM/yyyy HH.mm.ss",
                                 new CultureInfo("it-IT")), DateTime.ParseExact(y.TimeStamp, "dd/MM/yyyy HH.mm.ss",
                                 new CultureInfo("it-IT"))));
            String script = "";
            //script+="google.load('visualization', '1', {packages:['corechart']});";
            //script+="google.setOnLoadCallback(drawChart);";
            script +="function drawChart() {";
            script += "var data = google.visualization.arrayToDataTable([";
            script += "['Timestamp', 'Temperature (C)', 'Humidity (%)'],";
            foreach (Response res in list)
            {
                String[] firstsplit = res.TimeStamp.Split(' ');
                String[] secondsplit = firstsplit[0].Split('/');
                String[] thirdsplit = firstsplit[1].Split('.');
                Convert.ToDouble("9.2", CultureInfo.GetCultureInfo("it-IT"));
                Int32 month = (Convert.ToInt32(Convert.ToDouble(secondsplit[1], CultureInfo.GetCultureInfo("it-IT")))) - 1;
                script += "[new Date(" + secondsplit[2] + "," + month + "," + secondsplit[0] + "," + thirdsplit[0] + "," + thirdsplit[1] + "," + thirdsplit[2] + "),";
                //script += "['" + res.TimeStamp + "',";
                Convert.ToDouble("9.2", CultureInfo.GetCultureInfo("it-IT"));
                script += (Convert.ToInt32(Convert.ToDouble(res.Temperature, CultureInfo.GetCultureInfo("it-IT")))).ToString() + ",";
                script += (Convert.ToInt32(Convert.ToDouble(res.Humidity, CultureInfo.GetCultureInfo("it-IT"))).ToString()) + "],";
            }
            script+="]);";
            script+="var options = {";
            script += "legend: { position: 'top', alignment: 'center'},";
            script+="width: 700, height: 350,";
            script+="hAxis: { title: 'Datetime', format: 'dd/MMM HH:mm', textStyle: {fontSize:10,italic:false}, titleTextStyle: {fontSize:15,bold:true,italic:false}},";
            script+="vAxis: {title: 'Temperature (C) Humidity (%)', textStyle: {fontSize:15,italic:false}, titleTextStyle: {fontSize:15,bold:true,italic:false}},";
            script+="colors: ['#AB0D06', '#007329'],";
            script+="backgroundColor: '#f1f8e9'";
            script+="};";
            script += "var chart = new google.visualization.LineChart(document.getElementById('chart_div'));";
            script+="chart.draw(data, options);";
            script += "};";
            script += "drawChart();";
            this.ScriptGrafico+=script;
            return true;
        }

        public bool CreateScript2(List<Response> list)
        {
            if (list == null || list.Count == 0)
            {
                return false;
            }
            String script = "";
            list.Sort((x, y) => DateTime.Compare(DateTime.ParseExact(x.TimeStamp, "dd/MM/yyyy HH.mm.ss",
                                 new CultureInfo("it-IT")), DateTime.ParseExact(y.TimeStamp, "dd/MM/yyyy HH.mm.ss",
                                 new CultureInfo("it-IT"))));
            //script+="google.load('visualization', '1', {packages:['corechart']});";
            //script+="google.setOnLoadCallback(drawChart);";
            script += "function drawChart2() {";
            script += "var data = new google.visualization.DataTable();";
            script += "data.addColumn('datetime', 'Timestamp');";
            script += "data.addColumn('number', 'Wind');";
            script += "data.addRows([";
            foreach (Response res in list)
            {
                String[] firstsplit = res.TimeStamp.Split(' ');
                String[] secondsplit = firstsplit[0].Split('/');
                String[] thirdsplit = firstsplit[1].Split('.');
                Convert.ToDouble("9.2", CultureInfo.GetCultureInfo("it-IT"));
                Int32 month = (Convert.ToInt32(Convert.ToDouble(secondsplit[1], CultureInfo.GetCultureInfo("it-IT")))) - 1;
                script += "[new Date(" + secondsplit[2] + "," + month + "," + secondsplit[0] + "," + thirdsplit[0] + "," + thirdsplit[1] + "," + thirdsplit[2] + "),";
                Convert.ToDouble("9.2", CultureInfo.GetCultureInfo("it-IT"));
                script += (Convert.ToInt32(Convert.ToDouble(res.Wind, CultureInfo.GetCultureInfo("it-IT"))).ToString()) + "],";
            }
            script += "]);";
            script += "var options = {";
            script += "legend: { position: 'none' },";
            script += "width: 700, height: 350,";
            script += "hAxis: { title: 'Datetime', format: 'dd/MMM HH:mm', textStyle: {fontSize:10,bold:false,italic:false}, titleTextStyle: {fontSize:15,bold:true,italic:false}},";
            script += "vAxis: {title: 'Wind (Km/h)', textStyle: {fontSize:15,bold:false,italic:false}, titleTextStyle: {fontSize:15,bold:true,italic:false}},";
            script += "backgroundColor: '#f1f8e9'";
            script += "};";
            script += "var chart = new google.visualization.LineChart(document.getElementById('chart_div_2'));";
            script += "chart.draw(data, options);";
            script += "};";
            script += "drawChart2();";
            this.ScriptGrafico += script;
            return true;
        }
    }
}