﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Nos.Master" CodeBehind="NosUI.aspx.cs" Inherits="NosWebApp.WebForm1" %>
<asp:Content ID="BodyContent" ClientIDMode="Static" ContentPlaceHolderID="MainContent" runat="server">

    
    <div>
        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
            <ContentTemplate>

    <div id="body">
        <div>
			<ul id="navigation">
				<li class="current">
					<a class="link1">Demo</a>
				</li>
				<li>
					<a class="link2">Security</a>
				</li>
				<li>
					<a class="link1">Data</a>
				</li>
				<li>
					<a class="link2">Node.js</a>
				</li>
				<li>
					<a class="link1">MongoDB</a>
				</li>
			</ul>
			
               <div id="home">
				<h1><span>Data Presentation</span></h1>
                <h1>Campodenno Station</h1><br />
				<h2>Data Counter Analysis</h2><br /><br />
				
                <div class="section">	
                <table>
                    <tr>
                        <td style="text-align:center; width:180px; font-family: Helvetica, Arial, sans-serif; font-size:22px;">
                            <b>Elaborated Data</b><br />
                            <asp:Label ID="lblCountData" runat="server" style="font-size:24px; padding-top:15px;"></asp:Label>
                        </td>
                        <td style="text-align:center; width:180px; font-family: Helvetica, Arial, sans-serif; font-size:22px">
                            <b>Received Data</b><br />
                            <asp:Label ID="lblCountRawData" runat="server" style="font-size:24px; padding-top:15px;"></asp:Label>
                        </td>
                    </tr>
                </table>
                <br />
                <h2>Security Parameters Setting</h2><br /><br />
            </div>
           </div>
            <div id="tablegraph">
                <table>
                    <tr>
                        <td>
                <b>Confidentiality</b>
                <asp:TextBox ID="Confidentiality_bound" runat="server"></asp:TextBox>
                <asp:TextBox ID="Confidentiality" runat="server"></asp:TextBox>
                <ajaxToolkit:SliderExtender ID="Confidentiality_SliderExtender"  BoundControlID="Confidentiality_bound" runat="server" Enabled="True" Maximum="10" Steps="10" Minimum="0" TargetControlID="confidentiality">
                </ajaxToolkit:SliderExtender>
                        </td>
                        <td>
                <b>Integrity</b>
                <asp:TextBox ID="Integrity_bound" runat="server"></asp:TextBox>
                <asp:TextBox ID="Integrity" runat="server"></asp:TextBox>
                <ajaxToolkit:SliderExtender ID="Integrity_SliderExtender"  BoundControlID="Integrity_bound" runat="server" Enabled="True" Maximum="2" Steps="3" Minimum="0" TargetControlID="Integrity">
                </ajaxToolkit:SliderExtender>
                        </td>
                        <td>
                <b>Privacy</b>              
                <asp:TextBox ID="Privacy_bound" runat="server"></asp:TextBox>
                <asp:TextBox ID="Privacy" runat="server"></asp:TextBox>
                <ajaxToolkit:SliderExtender ID="SliderExtender1"  BoundControlID="Privacy_bound" runat="server" Enabled="True" Maximum="10" Steps="10" Minimum="0" TargetControlID="Privacy">
                </ajaxToolkit:SliderExtender>
                        </td>
                        <td>
                <b>Authenticity</b>
                <asp:TextBox ID="Authenticity_bound" runat="server"></asp:TextBox>
                <asp:TextBox ID="Authenticity" runat="server"></asp:TextBox>
                <ajaxToolkit:SliderExtender ID="SliderExtender2"  BoundControlID="Authenticity_bound" runat="server" Enabled="True" Maximum="1" Steps="1" Minimum="0" TargetControlID="Authenticity">
                </ajaxToolkit:SliderExtender>
                        </td>
                    </tr>
</table>
                <table>
                    <tr>
                        <td>
                <b>Completeness</b>
                <asp:TextBox ID="Completeness_bound" runat="server"></asp:TextBox>
                <asp:TextBox ID="Completeness" runat="server"></asp:TextBox>
                <ajaxToolkit:SliderExtender ID="SliderExtender3"  BoundControlID="Completeness_bound" runat="server" Enabled="True" Maximum="10" Steps="10" Minimum="0" TargetControlID="Completeness">
                </ajaxToolkit:SliderExtender>
                        </td>
                        <td>
                <b>Freshness</b>
                <asp:TextBox ID="Freshness_bound" runat="server"></asp:TextBox>
                <asp:TextBox ID="Freshness" runat="server"></asp:TextBox>
                <ajaxToolkit:SliderExtender ID="SliderExtender4"  BoundControlID="Freshness_bound" runat="server" Enabled="True" Maximum="10" Steps="10" Minimum="0" TargetControlID="Freshness">
                </ajaxToolkit:SliderExtender>
                        </td>
                        <td>
                <b>Accuracy</b>
                <asp:TextBox ID="Accuracy_bound" runat="server"></asp:TextBox>
                <asp:TextBox ID="Accuracy" runat="server"></asp:TextBox>
                <ajaxToolkit:SliderExtender ID="SliderExtender5"  BoundControlID="Accuracy_bound" runat="server" Enabled="True" Maximum="10" Steps="10" Minimum="0" TargetControlID="Accuracy">
                </ajaxToolkit:SliderExtender>
                        </td>
                    </tr>
                </table>          
                <br /><br />
                <asp:Button CssClass="btn" ID="cmdSubmit" runat="server" Text="Confirm Research" OnClick="cmdSubmit_Click" />

                <br /><br />

                <script>
                  google.load('visualization', '1', { packages: ['corechart'] });
                </script>

                <asp:Panel ID="pnlErrore" runat="server" Visible="false">
                   <br /><br /><p>NO DATA AVAILABLE FOR THE REQUESTED SETTINGS!</p>
                </asp:Panel>

                <div id="chart_div"><br /></div>
                <br />
                <div id="chart_div_2"><br /></div>

				</div>
		</div>  

            </ContentTemplate>
            
        </asp:UpdatePanel>
      
    </div>
  
</asp:Content>