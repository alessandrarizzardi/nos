﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(NosWebApp.Startup))]
namespace NosWebApp
{
    public partial class Startup {
        public void Configuration(IAppBuilder app) {
            ConfigureAuth(app);
        }
    }
}
