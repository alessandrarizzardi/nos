﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Json;


namespace NosWebApp
{
    [DataContract]
    public class ResponseCountService
    {
        [DataMember(Name = "count")]
        public String Count{get;set;}
    }
}