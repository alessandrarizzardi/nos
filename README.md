# README #

### About the Repository ###

* The project contains Node, Nos, Server, and NosWebApp applications
* Version 1.0

### Guidelines ###

* Create a MongoDB database with the following collections: rawdata, data, sources
* Insert in source collection some sources, in the following way (as an example):

{
    "id": 1,
    "type": "WSN",
    "communication": "WIFI",
    "algorithm": {
        "type": "ECC",
        "score": 10
    },
    "key": "***"
}

* Set the proper addresses in \Nos\model\Config.js (by default Nos and Node run on the same machine)
* Set the proper Nos address (property: ipPortNos) in \Nos\NosWebApp\NosWebApp\Web.config
* Start the applications in the following order: Server (nodejs) -> Nos (nodejs) -> Node (nodejs) -> NosWebApp (iss)

### Who do I talk to? ###

* Alessandra Rizzardi, University of Insubria Varese (Italy), e-mail: alessandra.rizzardi@uninsubria.it