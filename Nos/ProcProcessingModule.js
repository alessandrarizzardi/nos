
var ProcProcessingModule=function() {
	this.execInterval=null;
    this.dataAccess=require("./ProcDataAccess.js");
    this.client=require("./ClientServer");
};


ProcProcessingModule.prototype.start=function(){
	this.execInterval=setInterval(function(app) {
		app.exec();
	}, 1000,this);
};


ProcProcessingModule.prototype.exec=function(){
	
	this.dataAccess.fetch();
	
	var send=function(pm){
		if(!pm.dataAccess.canRead()){
			setTimeout(send, 1000, pm);
		} else {
			var data=pm.dataAccess.data;
			if(data !== null) {
				pm.client.sendData(data);
			}
		}
	};
	
	try{
		send(this);
	}catch (e) {
			console.error("NOS - ProcProcessingModule - Error in exec - " + e);
	}
};


ProcProcessingModule.prototype.stop=function(){
	clearInterval(this.execInterval);
};


module.exports = new ProcProcessingModule();