
var ProcessingModule=function() {
	this.execInterval=null;
    this.dataAccess=require("./DataAccess.js");
    
    this.analyzerVector= []; // data analyzers
    this.analyzerVector.push(require("./analyzers/SourceAnalysis"));
    this.analyzerVector.push(require("./analyzers/SecurityAnalysis"));
    this.analyzerVector.push(require("./analyzers/QualityAnalysis"));
};


ProcessingModule.prototype.start=function(){
	this.execInterval=setInterval(function(app) {
		try{
			app.exec();
		}catch (e) {
			console.error("NOS - ProcessingModule - Error in start - " + e);
		}
	}, 1000,this);
};


ProcessingModule.prototype.exec=function(){
	
	this.dataAccess.fetch();
	
	var analyze=function(pm){
		if(!pm.dataAccess.canRead()){
			setTimeout(analyze, 1000, pm);
		} else {
			var rawdata=pm.dataAccess.data;
			if(rawdata !== null) {
				pm.dataAccess.saveData(rawdata,function(listAn,r_data){
					for(var i=0; i<pm.analyzerVector.length; i++){
						pm.analyzerVector[i].analysis(r_data);
					}
				},pm.analyzerVector);
			}
		}
	};
	analyze(this);
};


ProcessingModule.prototype.stop=function(){
	clearInterval(this.execInterval);
};


module.exports = new ProcessingModule();