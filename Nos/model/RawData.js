
var RawData=function(){
	this.originalData=null;
};

RawData.prototype.setDataMap=function(dataMap){
	this.originalData=dataMap;
};

RawData.prototype.getPK=function(){
	if(this.originalData!==null){
		return this.originalData._id;
	}
	return -1;
};

RawData.prototype.getId=function(){
	if(this.originalData!==null){
		return this.originalData.Response.result.measures.sensor[0].id;
	}
};

RawData.prototype.getSourceid=function(){
	return  this.originalData.IdSource;
};

RawData.prototype.getDatatype=function(){
	return "json";
};

RawData.prototype.getDatacontent=function(){
	return "json";
};

RawData.prototype.getTimestamp=function(){
	if(this.originalData!==null){
		return this.originalData.Response.info.updated;
	}
};

RawData.prototype.getTemperature=function(){
	if(this.originalData!==null){
		return this.originalData.Response.result.measures.sensor[0].measure[0].value;
	}
};

RawData.prototype.getHumidity=function(){
	if(this.originalData!==null){
		return this.originalData.Response.result.measures.sensor[0].measure[1].value;
	}
};

RawData.prototype.getWind=function(){
	if(this.originalData!==null){
		return this.originalData.Response.result.measures.sensor[0].measure[3].value;
	}
};

module.exports = new RawData();