
var Data=function(){
	this.id=0;
	this.sourceid="";
	this.sourcetype="";
	this.sourcecommunicationmode="";
	this.datatype="";
	this.datacontent="";
	this.securityscore = {};
	this.qualityscore = {};
	this.timestamp="";
	this.temperature="";
	this.humidity="";
	this.wind="";
	//this.power="";
	this.processed=0; //0 = data already sent to server, 1 = data not sent to server
};

module.exports = new Data();