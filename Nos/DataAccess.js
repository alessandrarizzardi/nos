
var DataAccess=function() {
	var config = require("./model/Config.js");
	this.db = require('mongodb').MongoClient;
	var dataAccess=this;
	this.db.connect(config.mongodbconnection,function(err,con){
		if(err){
			console.dir(err);
		} else {
			dataAccess.db=con;
			console.dir("NOS - DEBUG - Connession completed - DataAccess!");
		}
	});
	this.data="";
	this.finish=true;
};


DataAccess.prototype.fetch=function(){
	this.finish=false;
	var dbselect=this.db.collection("rawdata");
	var dataAccess=this;
	
	dbselect.find({ processed : null }).toArray(function(err, rows) {
		if(rows.length > 0) {
			var row=rows[0];
			dataAccess.data=require("./model/RawData.js");
			dataAccess.data.setDataMap(row);
			console.log("NOS - DEBUG 1 - DataAccess - extracted row: " + JSON.stringify(dataAccess.data));
			dataAccess.markAsProcessed(dataAccess.data, dbselect);
		} else {
			dataAccess.data = null;
		}
		dataAccess.finish=true;
	});
    return true;
};


DataAccess.prototype.canRead=function(){
	return this.finish;
};


DataAccess.prototype.markAsProcessed=function(data, dbselect){
	dbselect.update(
			{ '_id' : data.getPK()},
			{ $set: { 'processed': 1 } },
			function (err, result) {
				if(err){
					throw err;
				}
				console.log("NOS - DEBUG 2 - DataAccess - markAsProcessed row: " + JSON.stringify(data));
			});
};


DataAccess.prototype.saveResult=function(rawdata, result_json){
	this.db.collection('data', function(err, collection) {
		collection.update(
				{ 'id_rawdata' : rawdata.getPK()},
				{ $set: result_json },
				function (err, result) {
					if(err){
						throw err;
					}
					console.log("NOS - DEBUG 4 - DataAccess - inserted result from analyzer processed row: " + JSON.stringify(result_json));
				});
	});
	
};


DataAccess.prototype.saveData=function(rawdata,fun,arg){
	this.db.collection('data', function(err, collection) {
		var doc = {
				"id_rawdata" : rawdata.getPK(),
				"id" : rawdata.getId(),
				"sourceid" : rawdata.getSourceid(),
				"datatype" : rawdata.getDatatype(),
				"datacontent" : rawdata.getDatacontent(),
				"timestamp" : rawdata.getTimestamp(),
				"temperature" : rawdata.getTemperature(),
				"humidity" : rawdata.getHumidity(),
				"wind" : rawdata.getWind(),
				"processed" : 0
		};
		collection.insert(doc, function() {
			//db.close();
			fun(arg,rawdata);
		});
	});
	console.log("NOS - DEBUG 3 - DataAccess - inserted processed row: " + JSON.stringify(rawdata));
};


DataAccess.prototype.insertData=function(data,res){
    this.db.collection("rawdata", function(err, collection) {
        collection.insert(data, {safe:true}, function(err, result) {
            if (err) {
                if(res!==null){
                    res.send({'error':'An error has occurred'});
                }
            } else {
                console.log("NOS - DEBUG 0 - DataAccess - inserted new row: " + JSON.stringify(result[0]));
                if(res){
                	try{
                		res.send(result[0]);
                	}catch (e) {
                		console.error("NOS - DataAccess - error in InsertData - " + e);
                	}
                }
            }
        });
    });
};


DataAccess.prototype.getData=function(req_q,res){
    this.db.collection("data", function(err, collection) {
    	
    	/*var completeness = parseInt(req_q.completeness, 10);
    	var freshness = parseInt(req_q.freshness, 10);
    	var accuracy = parseInt(req_q.accuracy, 10);*/
    	
        var json_q={
        		"securityscore.confidentiality": {"$gte": parseInt(req_q.confidentiality, 10)},
        		"securityscore.integrity":{"$gte": parseInt(req_q.integrity, 10)},
        		"securityscore.privacy": {"$gte": parseInt(req_q.privacy, 10)},
        		"securityscore.authentication": {"$gte": parseInt(req_q.authentication, 10)},
        		"qualityscore.completeness": {"$gte": parseInt(req_q.completeness, 10)},
        		"qualityscore.freshness": {"$gte": parseInt(req_q.freshness, 10)},
        		"qualityscore.accuracy": {"$gte": parseInt(req_q.accuracy, 10)}
        };
        console.log("NOS - DEBUG - DataAccess - getData: EXECUTED QUERY - " + JSON.stringify(json_q));
        
        collection.find(json_q).toArray(function(err, rows){
        	  if (err) {
                  if(res!==null){
                      res.send({'error':'An error has occurred'});
                  }
              } else {
            	   if(res){
	                   	try{ 
	                   		res.send(rows);
	                   	}catch (e) {
	                   		console.error("NOS - DataAccess - error in getData - " + e);
	                   	}
            	   }
              }  
        });
    });
};
DataAccess.prototype.getDataCount=function(res){
    this.db.collection("data", function(err, collection) {
    	console.log("NOS - DEBUG - DataAccess - getDataCount: EXECUTED QUERY");
       
	        collection.count(function(err, count) {
	      	  if (err) {
	                if(res!==null){
	                    res.send({'error':'An error has occurred'});
	                }
	            } else {
	          	   if(res){
		                   	try{
		                   		var rows={
		                   				"count":count
		                   		};
		                   		  res.send(rows);
		                   	}catch (e) {
		                   		console.error("NOS - DataAccess - error in getDataCount - " + e);
		                   	}
	          	   }
	            }
	        });
        });
};

DataAccess.prototype.getRawDataCount=function(res){
    this.db.collection("rawdata", function(err, collection) {
      
    		console.log("NOS - DEBUG - DataAccess - getDataRawCount: EXECUTED QUERY");
       
	        collection.count(function(err, count) {
	      	  if (err) {
	                if(res!==null){
	                    res.send({'error':'An error has occurred'});
	                }
	            } else {
	          	   if(res){
		                   	try{
		                   		var rows={
		                   				"count":count
		                   		};
		                   		  res.send(rows);
		                   	}catch (e) {
		                   		console.error("NOS - DataAccess - error in getDataRawCount - " + e);
		                   	}
	          	   }
	            }
	        });
        });
};

module.exports=new DataAccess();