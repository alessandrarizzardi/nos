
var PresentationDataAccess=function() {
	var config = require("../model/Config.js");
	this.db = require('mongodb').MongoClient;
	var dataAccess=this;
	this.db.connect(config.mongodbconnection,function(err,con){
		if(err){
			console.dir(err);
		} else {
			dataAccess.db=con;
			console.dir("NOS - DEBUG - Connession completed - PresentationDataAccess!");
			dataAccess.fetch();
		}
	});
	this.datas=new Array();
	this.dataCount=0; //counter for data
	this.rawdataCount=0; //counter for raw data
	this.finish=false;
};


PresentationDataAccess.prototype.fetch=function(){
	this.finish=false;
	var dbselect;
	var dbSelect_rawdata;
	try{
		dbselect=this.db.collection("data");
		dbSelect_rawdata=this.db.collection("rawdata");
	}catch (e) {
		console.error("NOS - PresentationDataAccess - Error in fetch - " + e);
		return false;
	}

	var dataAccess=this;
	dbselect.find({ processed : 1 }).toArray(function(err, rows) {
		dataAccess.datas=rows;
		this.dataCount=dataAccess.datas.length;
		var dataToGraph="";
		for(var i=0; i<dataAccess.datas.length; i++){
			dataToGraph += "['" + dataAccess.datas[i].timestamp + "'," + parseInt(dataAccess.datas[i].temperature,10) + "," + parseInt(dataAccess.datas[i].humidity,10) + "]";
			if(i<(dataAccess.datas.length-1)){
				dataToGraph +=",";
			}
		}
		dataAccess.datas=dataToGraph;
		dataAccess.finish=true;
	});
	
	dbSelect_rawdata.count(function(err, count) {
		this.rawdataCount= count;
    });
    return true;
};


PresentationDataAccess.prototype.canRead=function(){
	return this.finish;
};


module.exports=new PresentationDataAccess();