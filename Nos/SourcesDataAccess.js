
var SourcesDataAccess=function() {
	var config = require("./model/Config.js");
	this.db = require('mongodb').MongoClient;
	var SourcesDataAccess=this;
	this.db.connect(config.mongodbconnection,function(err,con){
		if(err){
			console.dir(err);
		} else {
			SourcesDataAccess.db=con;
			console.dir("NOS - DEBUG - Connession completed - SourcesDataAccess!");
		}
	});
	this.data="";
	this.finish=true;
};


SourcesDataAccess.prototype.fetch=function(idSource){
	this.finish=false;
	var dbselect=this.db.collection("sources");
	var SourcesDataAccess=this;
	
	dbselect.find({ id : idSource}).toArray(function(err, rows) {
		if(rows && rows.length > 0) {
			var row=rows[0];
			SourcesDataAccess.data=row;
			console.log("NOS - DEBUG 2 - SourcesDataAccess - found record: " + JSON.stringify(SourcesDataAccess.data));
		} else {
			SourcesDataAccess.data = null;
		}
		SourcesDataAccess.finish=true;
	});
    return true;
};

SourcesDataAccess.prototype.canRead=function(){
	return this.finish;
};


SourcesDataAccess.prototype.insertData=function(data,res){
    this.db.collection("sources", function(err, collection) {
        collection.insert(data, {safe:true}, function(err, result) {
            if (err) {
                if(res!==null){
                    res.send({'error':'An error has occurred'});
                }
            } else {
                console.log('NOS - DEBUG 1 - SourcesDataAccess - inserted new row: ' + JSON.stringify(result[0]));
                if(res){
                    res.send(result[0]);
                }
            }
        });
    });
};


module.exports=new SourcesDataAccess();