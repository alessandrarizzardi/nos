
var PortFromNodeToNos=function(){
    this.service=require("express");
    this.app=this.service();
    var express=this.service;
    
    this.app.configure(function(){
        this.use(express.bodyParser());
        this.use(this.router);
    });
    
    this.app.get('/data', function (req, res) {
        res.send('Ping...');
    });
    
    this.app.post('/insert',this.InsertData);
    
    //REST services
    this.app.get('/get',this.getData); //data
    this.app.get('/getDataCount',this.getDataCount); //counter data
    this.app.get('/getRawDataCount',this.getRawDataCount); //counter rawdata
    
};


PortFromNodeToNos.prototype.InsertData=function(req, res) {
	try{
		var data = req.body;
		if (data.Response.status !== "OK"){
			res.send("ERROR: " + data);
		} else {
			console.log('NOS - DEBUG - PortFromNodeToNos - adding data: ' + JSON.stringify(data));
			var dataAccess=require("./DataAccess");
			dataAccess.insertData(data,res);
		}
	}catch (e) {
		console.error("NOS - PortFromNodeToNos - Error in InsertData - " + e);
	}
};


PortFromNodeToNos.prototype.getData=function(req, res) {
	try{
		var req_filter= req.query;
		var dataAccess=require("./DataAccess");
		dataAccess.getData(req_filter,res);
	}catch (e) {
		console.error("NOS - PortFromNodeToNos - Error in getData " + e);
	}
};


PortFromNodeToNos.prototype.getDataCount=function(req, res) {
	try{
		var dataAccess=require("./DataAccess");
		dataAccess.getDataCount(res);
	}catch (e) {
		console.error("NOS - PortFromNodeToNos - Error in getDataCount " + e);
	}
};


PortFromNodeToNos.prototype.getRawDataCount=function(req, res) {
	try{
		var dataAccess=require("./DataAccess");
		dataAccess.getRawDataCount(res);
	}catch (e) {
		console.error("NOS - PortFromNodeToNos - Error in getRawDataCount " + e);
	}
};


PortFromNodeToNos.prototype.start=function(){
	this.app.listen(3000);
};


module.exports = new PortFromNodeToNos();