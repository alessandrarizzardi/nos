
var ProcDataAccess=function() {
	var config = require("./model/Config.js");
	this.db = require('mongodb').MongoClient;
	var dataAccess=this;
	this.db.connect(config.mongodbconnection,function(err,con){
		if(err){
			console.dir(err);
		} else {
			dataAccess.db=con;
			console.dir("NOS - DEBUG - Connession completed - ProcDataAccess!");
		}
	});
	this.data="";
	this.finish=true;
};


ProcDataAccess.prototype.fetch=function(){
	this.finish=false;
	var dbselect;
	try{
		dbselect=this.db.collection("data");
	}catch (e) {
		console.error("NOS - ProcDataAccess - error in fetch - " + e);
		return false;
	}
	
	var dataAccess=this;
	dbselect.find({ processed : 0 }).toArray(function(err, rows) {
		if(rows && rows.length > 0) {
			var row=rows[0];
			dataAccess.data=require("./model/Data.js");
			dataAccess.data._id=row._id;
			dataAccess.data.id=row.id;
			dataAccess.data.sourceid=row.sourceid;
			dataAccess.data.sourcetype=row.sourcetype;
			dataAccess.data.sourcecommunicationmode=row.sourcecommunicationmode;
			dataAccess.data.datatype=row.datatype;
			dataAccess.data.datacontent=row.datacontent;
			dataAccess.data.securityscore=row.securityscore;
			dataAccess.data.qualityscore=row.qualityscore;
			dataAccess.data.timestamp=row.timestamp;
			dataAccess.data.temperature=row.temperature;
			dataAccess.data.humidity=row.humidity;
			dataAccess.data.wind=row.wind;
			//dataAccess.data.power=row.power;
			console.log("NOS - DEBUG 4 - ProcDataAccess - extracted row: " + JSON.stringify(dataAccess.data));
			dataAccess.markAsProcessed(dataAccess.data, dbselect);
		} else {
			dataAccess.data = null;
		}
		dataAccess.finish=true;
	});
    return true;
};


ProcDataAccess.prototype.canRead=function(){
	return this.finish;
};


ProcDataAccess.prototype.markAsProcessed=function(data, dbselect){
	dbselect.update(
			{ '_id' : data._id },
			{ $set: { 'processed': 1 } },
			function (err, result) {
				if(err){throw err;}
				console.log("NOS - DEBUG 5 - ProcDataAccess - markAsProcessed row: " + JSON.stringify(data));
			});
};


module.exports=new ProcDataAccess();