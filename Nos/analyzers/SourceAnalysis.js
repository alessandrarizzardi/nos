
var SourceAnalysis=function(){
	this.dataAccess=require("../SourcesDataAccess.js");
	this.raw_dataAccess=require("../DataAccess.js");
};


SourceAnalysis.prototype.analysis=function(data){
	
	console.log("NOS - DEBUG - SourceAnalysis -> analyzing data..." + " id " + data.originalData._id);
	
	this.dataAccess.fetch(data.getSourceid());
	
	var analyze=function(pm){
		if(!pm.dataAccess.canRead()){
			setTimeout(analyze, 1000, pm);
		} else {
			var infoSource=pm.dataAccess.data;
			var result;
			if(infoSource !== null) { //registered sources
				result={
						"sourcetype": "registered",
						//"sourcecommunicationmode": ""
				};
				pm.raw_dataAccess.saveResult(data,result);
				
			} else { //no registered sources
				result={
						"sourcetype": "no registered",
						//"sourcecommunicationmode": ""
				};
				pm.raw_dataAccess.saveResult(data,result);
			}
		}
	};
	analyze(this);
};


module.exports=new SourceAnalysis();