
var SecurityAnalysis=function(){
	 this.dataAccess=require("../SourcesDataAccess.js");
	 this.raw_dataAccess=require("../DataAccess.js");
};


SecurityAnalysis.prototype.analysis=function(data){
	
	console.log("NOS - DEBUG - SecurityAnalysis -> analyzing data..."  + " id " + data.originalData._id);
	
	this.dataAccess.fetch(data.getSourceid());
	
	var analyze=function(pm){
		if(!pm.dataAccess.canRead()){
			setTimeout(analyze, 1000, pm);
		} else {
			var infoSource=pm.dataAccess.data;	
			var result;
			
			if(infoSource !== null) { //registered sources	
				var integrity= Math.floor((Math.random() * 100) + 1); //simulation of data integrity violation
				
				if(integrity > 80){
					integrity=0;
				} else {
					integrity=2;
				}
				
				var privacy = Math.floor((Math.random() * 100) + 1);
				if(privacy > 70){
					privacy=0;
				} else {
					privacy=infoSource.algorithm.score;
				}
				
				result={
						"securityscore": {
							"confidentiality":infoSource.algorithm.score,
							"integrity":integrity,
							"privacy":privacy,
							"authentication":1
						}	
				};
				pm.raw_dataAccess.saveResult(data,result);
			} else { //no registered sources
				result={
						"securityscore": {
							"confidentiality":0,
							"integrity": 1,
							"privacy": 0,
							"authentication":0
						}	
				};
				pm.raw_dataAccess.saveResult(data,result);
			}
		}
	};
	analyze(this);
};


module.exports=new SecurityAnalysis();