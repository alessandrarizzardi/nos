
var QualityAnalysis=function(){
	 this.dataAccess=require("../SourcesDataAccess.js");
	 this.raw_dataAccess=require("../DataAccess.js");
};


QualityAnalysis.prototype.analysis=function(data){
	
	console.log("NOS - DEBUG - QualityAnalysis -> analyzing data..."  + " id " + data.originalData._id);
	
	this.dataAccess.fetch(data.getSourceid());
	
	var analyze=function(pm){
		if(!pm.dataAccess.canRead()){
			setTimeout(analyze, 1000, pm);
		} else {
			var infoSource=pm.dataAccess.data;	
			var result;
			
			if(infoSource !== null) { //registered sources	
				
				result={
						"qualityscore": {
							"completeness":infoSource.quality.completeness,
							"freshness":infoSource.quality.freshness,
							"accuracy":infoSource.quality.accuracy,
						}	
				};
				pm.raw_dataAccess.saveResult(data,result);
			} else { //no registered sources
				result={
						"qualityscore": {
							"completeness":0,
							"freshness": 0,
							"accuracy": 0
						}	
				};
				pm.raw_dataAccess.saveResult(data,result);
			}
		}
	};
	analyze(this);
};


module.exports=new QualityAnalysis();