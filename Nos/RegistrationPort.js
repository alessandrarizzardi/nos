
var RegistrationPort = function(){
	 this.service=require("express");
	 this.app=this.service();
	 var express=this.service;
	 
	 this.app.configure(function(){
		 this.use(express.bodyParser());
	     this.use(this.router);
	 });
	 
	 this.app.get('/registration', function (req, res) {
		 res.send('Ping...');
	 });
	 
	 this.app.post('/registration/newsource',this.NewSource);
};


RegistrationPort.prototype.NewSource=function(req, res){
	try{
	    var data=req.body;  
	    var dAccess=require("./SourcesDataAccess");
	    dAccess.insertData(data,res);
	}catch (e) {
		console.error("NOS - RegistrationPort - Error in NewSource - " + e);
		res.send("Registration Service Error");
	}
};


RegistrationPort.prototype.start=function(){
	this.app.listen(4001);
};


module.exports = new RegistrationPort();