
var ClientToServer=function() {
	this.config = require("./model/Config.js");
	var Client = require('node-rest-client').Client;
	this.client = new Client();
};


ClientToServer.prototype.sendData=function(data) {
	try {
		var args={
			data:data,
			headers:{ "Content-Type" : "application/json" }
		};
		
		this.client.post(this.config.clientServerConfig, args, function(data,response) {
			//console.log(data);
			//console.log(response);
		});
	} catch (e) {
		console.error("NOS - ClientServer - Error in sending data to server - " + e);
	}
};


module.exports=new ClientToServer();