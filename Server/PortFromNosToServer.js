
var PortFromNosToServer=function(){
    this.service=require("express");
    this.app=this.service();
    var express=this.service;
    
    this.app.configure(function(){
        this.use(express.bodyParser());
        this.use(this.router);
    });
    
    this.app.get('/data', function (req, res) {
        res.send('Ping...');
    });
    
    this.app.post('/pull',this.pullData);
};


PortFromNosToServer.prototype.pullData=function(req, res) {
    var data=req.body;
    console.log("SERVER - DEBUG - PortFromNosToServer - received row: " + JSON.stringify(data));
    res.send("OK");
};


PortFromNosToServer.prototype.start=function(){
	this.app.listen(4000);
};


var server=new PortFromNosToServer();


server.start();