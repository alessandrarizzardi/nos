
var GetDataFromWeb=function(){
	var Client=require('node-rest-client').Client;
	this.client=new Client();
};


GetDataFromWeb.prototype.fetch=function(){
	
	var args = {
			headers:{"Content-Type": "application/json"}
	};
	
	this.client.get("http://campodenno.taslab.eu/stazioni/json?id=CMD001", args, function(data,response) {
		try{
			var dParser=JSON.parse(data);
			dParser["IdSource"]=Math.floor((Math.random() * 7) + 1);
			var sendData=require("./SendData");
			var sendDataObj=JSON.stringify(dParser);
			sendData.send(sendDataObj);
		}catch (e) {
			console.error("NODE - GetDataFromWeb - Error in receiving data from node - " + e);
		}
	});	
};


module.exports = new GetDataFromWeb();