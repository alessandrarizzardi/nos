
var SendData=function(){
	var Client=require('node-rest-client').Client;
	this.client=new Client();
};


SendData.prototype.send=function(dataJson){
	
	var args = {
			data: dataJson,
			headers:{"Content-Type": "application/json"}
	};
	
	this.client.post("http://localhost:3000/insert", args, function(data,response) {
		console.log("NODE - DEBUG - SendData - new row: " + JSON.stringify(data)); //In this example, nodes run the same server of the NOS
	});
};


module.exports = new SendData();